/**
 * 
 */
package com.corejava.codility;

import java.util.Arrays;

import com.corejava.utility.Utility;

/**
 * @author Bhabadyuti Bal
 *
 */
public class OddOccurancesInArray {
	
	
	public int smallestPossibleInteger(int[] A) {
		int val = 0;
		int arr[] = A;
		Arrays.sort(A);
		if((1 + A[0])/2 > 1) {
			val = 1;
		} else if((1 + A[0])/2 == 1) {
			for(int i=A[0];i<=A[A.length-1];i++) {
				if(A[i] != i) {
					val = i;
				}
			}
			if(val != 0) {
				val = A[A.length-1] + 1;
			}
		}
		
		return val;
	}

	public int solution(int[] A) {
		int val = 0;
		if(A.length == 1)
			val = A[0];
		else {
			Arrays.sort(A);
			int count = 0;
			for(int i=0;i<A.length - 1;i++) {
				if(A[i] != A[i+1]) {
					count++;
					if((count >= 1) && (A.length - 1 == i+1)) {
						val = A[i+1];
					}
					if(count >= 2) {
						val = A[i];
					}
					continue;
				}
				count = 0;
			}
		}
		return val;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] A = new int[] { 9, 3, 3, 9, 3, 9, 7, 9, 7, 13, 13, 13, 17, 21, 21 };
		//int ele = new OddOccurancesInArray().solution(A);
		//System.out.println(ele);
		int arr[] = new int[]{-1, -3};
		int ele = new OddOccurancesInArray().smallestPossibleInteger(arr);
		System.out.println(ele);
	}

}
