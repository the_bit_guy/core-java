/**
 * 
 */
package com.corejava.codility;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Bhabadyuti Bal
 *
 */
public class Backbase {

	public static boolean isLong(String id) {
		boolean isParsable = false;
		try {
			Long.valueOf(id);
			isParsable = true;
		} catch (Exception e) {
			isParsable = false;
		}
		return isParsable;
	}

	Map<Long, Long> count(Map<String, UserStats>... visits) {
		Map<Long, Long> finalMap = new HashMap<>();
		if (visits != null) {
			List<Map<String, UserStats>> list = Arrays.asList(visits);
			list.stream().forEach(item -> {
				if (item != null) {
					item.entrySet().forEach(map -> {
						String key = map.getKey();
						UserStats userStats = map.getValue();
						if (isLong(key) && userStats != null && userStats.getVisitCount().isPresent()) {
							if (finalMap.size() > 0 && finalMap.containsKey(key)) {
								Long visit = userStats.getVisitCount().get();
								visit += finalMap.get(key);
								finalMap.put(Long.valueOf(key), visit);
							} else {
								finalMap.put(Long.valueOf(key), map.getValue().getVisitCount().get());
							}
						}
					});
				}
			});
		}
		return finalMap;
	}

	public static void main(String[] args) {
		Backbase backbase = new Backbase();
		Map<String, UserStats> entryDetails1 = new HashMap<>();
		UserStats userStats1 = new UserStats(Optional.empty());
		UserStats userStats2 = new UserStats(Optional.of(new Long(8)));
		entryDetails1.put("1000", userStats1);
		entryDetails1.put("1001", userStats2);

		Map<String, UserStats> entryDetails2 = new HashMap<>();
		UserStats userStats3 = new UserStats(Optional.empty());
		UserStats userStats4 = new UserStats(Optional.of(new Long(9)));
		entryDetails2.put("1003", userStats3);
		entryDetails2.put("1004", null);
		entryDetails2.put("1001", userStats4);

		Map<Long, Long> map = backbase.count(entryDetails1, entryDetails2);

		System.out.println(map);
	}

}
