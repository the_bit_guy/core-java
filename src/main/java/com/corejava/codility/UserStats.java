package com.corejava.codility;

import java.util.Optional;

public class UserStats {

	Optional<Long> visitCount;
	
	public UserStats() {
	}
	
	public UserStats(Optional<Long> visitCount) {
		this.visitCount = visitCount;
	}

	public Optional<Long> getVisitCount() {
		return visitCount;
	}

	public void setVisitCount(Optional<Long> visitCount) {
		this.visitCount = visitCount;
	}
}
