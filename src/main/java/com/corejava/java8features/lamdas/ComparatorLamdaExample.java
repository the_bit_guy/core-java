package com.corejava.java8features.lamdas;

import java.util.Comparator;

public class ComparatorLamdaExample {
	
	public static void java7Way() {
		Comparator<Integer> comparator = new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				return o1.compareTo(o2); //0 if o1 == o2, 1 if o1 > o2, -1 if o1 < o2
			}
		};
		System.out.println("Result of comparision : "+comparator.compare(5, 3));
	}
	
	public static void java8Way() {
		Comparator<Integer> comparator = (o1, o2) -> o1.compareTo(o2);
		System.out.println("Result of comparision : "+comparator.compare(5, 3));
	}
	
	public static void main(String[] args) {
		java7Way();
		java8Way();
	}

}
