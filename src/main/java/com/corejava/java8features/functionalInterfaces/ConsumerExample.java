/**
 * 
 */
package com.corejava.java8features.functionalInterfaces;

import java.util.List;
import java.util.function.Consumer;

import com.corejava.utility.StudentDB;
import com.corejava.vo.Student;

/**
 * @author Bhabadyuti Bal
 *
 */
public class ConsumerExample {
	
	
	public void doSomething(String s){
		System.out.println(s.toUpperCase());
	}
	
	public static void printNames() {
		
		Consumer<Student> consumer2 = (student) -> System.out.println(student);
		
		List<Student> allStudents = StudentDB.getAllStudents();
		/*
		 * forEach takes a consumer type and goes through the supplied Iterable
		 */
		allStudents.forEach(consumer2);
		Consumer<? super Student> action = student -> System.out.println(student.getName());
		allStudents.forEach(action);
		
		
	}
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		/*
		 * performing the below operation in java 7 way
		 */
		ConsumerExample consumerExample = new ConsumerExample();
		//consumerExample.doSomething("Java 9");
		
		//(s) -> System.out.println(s.toUpperCase())
		
		/*
		 * Consumer represents an operation that accepts a single input argument and 
		 * returns no result. Unlike most other functional interfaces, Consumer is 
		 * expected to operate via side-effects. 
		 */
		Consumer<String> consumer = (s) -> System.out.println(s.toUpperCase());
		consumer.accept("Java 8");
		
		printNames();
	}

}
