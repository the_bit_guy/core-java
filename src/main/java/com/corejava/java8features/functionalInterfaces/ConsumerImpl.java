package com.corejava.java8features.functionalInterfaces;

/*
 * In functional interface 
 */
@FunctionalInterface
public interface ConsumerImpl<T> {

	public abstract void accept(T t);
	
	default void fcvh() {
		System.out.println("");
	}

	static void ingMe() {
		
	}

}
