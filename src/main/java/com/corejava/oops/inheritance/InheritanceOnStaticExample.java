package com.corejava.oops.inheritance;

class Parent {
	
	static {
		System.out.println("Parent static block");
	}
}


public class InheritanceOnStaticExample extends Parent {

	static {
		System.out.println("Child static block");
	}
	
	public static void main(String[] args) {
		InheritanceOnStaticExample ob = new InheritanceOnStaticExample();
		
	}
	
}
