package com.corejava.utility;

import java.util.Arrays;
import java.util.List;

import com.corejava.vo.Student;

public class StudentDB {
	
	public static List<Student> getAllStudents(){
		
		Student student1 = new Student("Chhotu", 1, 6.0, "male", Arrays.asList("running","swimming","biking"));
		Student student2 = new Student("Pintu", 1, 6.0, "male", Arrays.asList("running","swimming","biking"));
		
		Student student3 = new Student("Baapun", 2, 6.0, "male", Arrays.asList("running","swimming","biking"));
		Student student4 = new Student("Rony", 3, 6.0, "male", Arrays.asList("running","swimming","biking"));
		
		Student student5 = new Student("Ross", 3, 6.0, "male", Arrays.asList("running","swimming","biking"));
		Student student6 = new Student("Rinku", 3, 6.0, "male", Arrays.asList("running","swimming","biking"));
		
		return Arrays.asList(student1, student2, student3, student4, student5, student6, null);
	}

}
