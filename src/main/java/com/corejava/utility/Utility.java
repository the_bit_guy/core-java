package com.corejava.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class Utility {
	
	
	static Connection crunchifyConn = null;
	static PreparedStatement crunchifyPrepareStat = null;
	
	
	public static Connection createDBConnection(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			//log("Congrats - Seems your MySQL JDBC Driver Registered!");
		} catch (ClassNotFoundException e) {
			//log("Sorry, couldn't found JDBC driver. Make sure you have added JDBC Maven Dependency Correctly");
			e.printStackTrace();
			return null;
		}
 
		try {
			// DriverManager: The basic service for managing a set of JDBC drivers.
			crunchifyConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "root");
			if (crunchifyConn != null) {
				//log("Connection Successful! Enjoy. Now it's time to push data");
			} else {
				//log("Failed to make connection!");
			}
		} catch (SQLException e) {
			//log("MySQL Connection Failed!");
			e.printStackTrace();
			return null;
		}
		return crunchifyConn;
	}
	

	public static void log(String msg) {
		System.out.println(msg);
		//logger.info(msg);
	}


	public static void printIntegerArray(int array[]) {
		System.out.println("Output Array...");
		System.out.print("[");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i]);
			if(i < array.length - 1) {
				System.out.print(", ");
			}
		}
		System.out.print("]");
	}

	public static int[] stringToArray(String input) {
		String[] split = input.split(",");
		int[] array = new int[split.length];
		System.out.println("Array Length: " + array.length);
		for (int i = 0; i < split.length; i++) {
			array[i] = Integer.parseInt(split[i]);
		}

		return array;
	}

	public static void main(String[] args) {
		List<String> list = null;
		//System.out.println(list.isEmpty());
	}

}
