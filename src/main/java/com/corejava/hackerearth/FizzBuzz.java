package com.corejava.hackerearth;

import java.util.Scanner;

public class FizzBuzz {

    public static void main(String args[]){
        Scanner s = new Scanner(System.in);
        int noOfTc = Integer.valueOf(s.nextLine());                 // Reading input from STDIN
        String[] inputs = s.nextLine().split(" ");
        for(int i=0;i<inputs.length;i++) {
            int intVal = Integer.valueOf(inputs[i]);
            for(int j=1;j<=intVal;j++) {
                if(j == (3*5)) {
                    System.out.println("FizzBuzz");
                } else if((j == 3) || (j == (3*(j/3)))) {
                    System.out.println("Fizz");
                } else if((j == 5) || (j == (5*(j/5)))) {
                    System.out.println("Buzz");
                } else if(j == (3*5)) {
                    System.out.println("FizzBuzz");
                } else {
                    System.out.println(j);
                }
            }
        }
    }
}
