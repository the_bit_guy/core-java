package com.corejava.hackerearth;

import java.util.ArrayList;
import java.util.Stack;

public class BSsequence {

    public static void printAllSubSeq(ArrayList<Integer> list) {
    	Integer[] array = list.toArray(new Integer[list.size()]);
    	for(int i=0;i<array.length;i++) {
    		Stack<Integer> stack = new Stack<>();
    		for(int j=0;j<array.length;j++) {
    			if(array[j] < array[i]) {
    				stack.push(array[j]);
        			System.out.println(stack);
    			}
    		}
    	}
    	//System.out.println(list);
    }

    public static void main(String[] args){
        int[] array = new int[]{2, 1, 3, 1, 6, 2};
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < array.length; i++) {
            list.add(array[i]);
            printAllSubSeq(list);
            System.out.println("===============");
        }
    }
}
