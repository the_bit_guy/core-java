package com.corejava.multithreading.test;

public class Test1 implements Runnable {
	
	@Override
	public void run() {
		process();
	}
	
	private static void process() {
		System.out.println("State - "+Thread.currentThread().getState());
		System.out.println("process method executed by thread: "+Thread.currentThread().getName());
	}
	
	public static void main(String[] args) {
		System.out.println(Thread.currentThread().getName());
		
		Thread thread1 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				process();
			}
			
		});
		System.out.println("State - "+thread1.getState());
		thread1.start();
		
		Thread thread2 = new Thread(new Runnable() {
			@Override
			public void run() {
				process();
			}
		});
		thread2.start();
		
	}

}
