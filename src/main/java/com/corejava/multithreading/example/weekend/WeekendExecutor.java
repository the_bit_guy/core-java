/**
 * 
 */
package com.corejava.multithreading.example.weekend;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author Bhabadyuti Bal
 *
 */
public class WeekendExecutor {
	
	
	public boolean doShopping() throws InterruptedException {
		System.out.println("Shopping...");
		try {
			throw new Exception();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread.sleep(5000);
		return true;
	}

	public boolean doCleaning() throws InterruptedException {
		System.out.println("Cleaning...");
		Thread.sleep(5000);
		return true;
	}

	public boolean doCooking() throws InterruptedException {
		System.out.println("Cooking...");
		Thread.sleep(50000);
		return true;
	}
	
	public List<Callable<Boolean>> getAllTasks() {
		Callable<Boolean> cleaning = () -> {
			return doCleaning(); 
		};
		
		Callable<Boolean> shopping = () -> {
			return doShopping(); 
		};
		
		Callable<Boolean> cooking = () -> {
			return doCooking(); 
		};
		
		List<Callable<Boolean>> weekendTasks = new ArrayList<>();
		weekendTasks.add(cleaning);
		weekendTasks.add(shopping);
		weekendTasks.add(cooking);
		return weekendTasks;
	}
	
	public void process() {
		ExecutorService executor = Executors.newFixedThreadPool(3);
		try {
			long startTime = System.currentTimeMillis();
			List<Future<Boolean>> allGuys = executor.invokeAll(getAllTasks());
			boolean isAllDone = false;
			for(Future<Boolean> guy : allGuys) {
				isAllDone &= guy.isDone();
			}
			if(isAllDone) {
				System.out.println("All tasks completed!");
			}
			long endTime = System.currentTimeMillis();
			System.out.println("Time taken: "+((endTime - startTime)/1000)+" secs");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new WeekendExecutor().process();
	}
}
