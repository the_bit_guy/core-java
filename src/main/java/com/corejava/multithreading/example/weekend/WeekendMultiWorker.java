package com.corejava.multithreading.example.weekend;

/**
 * 
 * @author Bhabadyuti Bal
 *
 */
public class WeekendMultiWorker {
	
	/*
	 * Here implementing 3 threads [cleaning, shopping, cooking]
	 * and running a while loop for main thread that keeps checking
	 * if the 3 threads completed their tasks.
	 * 
	 * As a result the time taken reduced to 5 secs.
	 */

	boolean isCleaningDone = false;
	boolean isShoppiingDone = false;
	boolean isCookingDone = false;

	public void doShopping() throws InterruptedException {
		System.out.println("Shopping...");
		System.out.println("Current thread: "+Thread.currentThread());
		Thread.sleep(5000);
		isShoppiingDone = true;
	}

	public void doCleaning() throws InterruptedException {
		System.out.println("Cleaning...");
		System.out.println("Current thread: "+Thread.currentThread());
		Thread.sleep(5000);
		isCleaningDone = true;
	}

	public void doCooking() throws InterruptedException {
		System.out.println("Cooking...");
		System.out.println("Current thread: "+Thread.currentThread());
		Thread.sleep(5000);
		isCookingDone = true;
	}

	Runnable shopping = () -> {
		try {
			doShopping();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	};

	Runnable cleaning = () -> {
		try {
			doCleaning();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	};

	Runnable cooking = () -> {
		try {
			doCooking();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	};

	public void process() throws InterruptedException {
		long startTime = System.currentTimeMillis();
		Thread clThread = new Thread(cleaning);
		clThread.setName("clThread");
		clThread.start();
		
		Thread shThread = new Thread(shopping);
		shThread.setName("shThread");
		shThread.start();
		
		Thread coThread = new Thread(cooking);
		coThread.setName("coThread");
		coThread.start();
		
		while (true) {
			if (isCookingDone) {
				System.out.println("cooking Completed!");
			}
			if (isCleaningDone) {
				System.out.println("cleaning Completed!");
			}
			if (isShoppiingDone) {
				System.out.println("shopping Completed!");
			}
			if (isCookingDone && isCleaningDone && isShoppiingDone) {
				System.out.println("All Completed!");
				break;
			}
			Thread.sleep(1);
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Time taken: "+((endTime - startTime)/1000)+" secs");
	}

	public static void main(String[] args) throws InterruptedException {
		new WeekendMultiWorker().process();
	}
}
