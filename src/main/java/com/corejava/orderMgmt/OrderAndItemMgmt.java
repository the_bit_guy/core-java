package com.corejava.orderMgmt;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OrderAndItemMgmt {
	
	public Set<String> getListOfItems() {
		 Set<String> items = new HashSet<>();
		 items.add("Clothes");
		 items.add("Books");
		 
		 return items;
	}
	
	
	public Set<Order> getListOfOrders() {
		 Set<Order> orders = new HashSet<>();
		 List<Item> list = new ArrayList<>();
		 list.add(new Item());
		 orders.add(new Order(list));
		 return orders;
	}

}
