package com.corejava.orderMgmt;

import java.io.Serializable;
import java.util.List;

public class Order implements Serializable {

	private List<Item> items;
	
	

	/**
	 * @param items
	 */
	public Order(List<Item> items) {
		this.items = items;
	}

	/**
	 * @return the items
	 */
	public List<Item> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(List<Item> items) {
		this.items = items;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Order [items=" + items + "]";
	}
	
	
 }
