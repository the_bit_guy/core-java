package com.corejava.orderMgmt;

import java.util.Scanner;

public class AppMain {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Login Required!");
		System.out.println("Enter your username");
		String username = sc.next();
		System.out.println("Enter your password");
		String password = sc.next();
		if(username.equals(password)) {
			System.out.println("Available Items");
			OrderAndItemMgmt orderAndItemMgmt = new OrderAndItemMgmt();
			System.out.println(orderAndItemMgmt.getListOfItems());
			System.out.println(orderAndItemMgmt.getListOfOrders());
		} else {
			System.out.println("Username & password strings has to be same");
			System.out.println("Please rerun the app to continue!!");
		}
	}

}
