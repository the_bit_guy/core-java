package com.corejava.hackerank;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.corejava.utility.Utility;

public class Solution {

	static String numbers = "0123456789";
	static String lower_case = "abcdefghijklmnopqrstuvwxyz";
	static String upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	static String special_characters = "!@#$%^&*()-+";

	// Complete the minimumNumber function below.
	static int minimumNumber(int n, String password) {
		// Return the minimum number of characters to make the password strong
		if (n < 6) {
			int num = checkMinRqNum(password);
			if (num + n >= 6)
				return num;
			else {
				return 6 - n;
			}
		} else {
			return checkMinRqNum(password);
		}
	}

	private static int checkMinRqNum(String password) {
		int reqNums = 0;
		List<Boolean> list = new ArrayList<>();
		boolean numFlag = false, lcFlag = false, ucFlag = false, scFlag = false;
		char[] arr = password.toCharArray();
		for (int i = 0; i < arr.length; i++) {
			String str = String.valueOf(arr[i]);
			if (numFlag == false && numbers.contains(str)) {
				numFlag = true;
			} else if (lcFlag == false && lower_case.contains(str)) {
				lcFlag = true;
			} else if (ucFlag == false && upper_case.contains(str)) {
				ucFlag = true;
			} else if (scFlag == false && special_characters.contains(str)) {
				scFlag = true;
			}
		}
		list.add(numFlag);
		list.add(lcFlag);
		list.add(ucFlag);
		list.add(scFlag);
		reqNums = Collections.frequency(list, false);
		return reqNums;
	}

	// Complete the camelcase function below.
	static int camelcase(String s) {
		int wordsCount = 1;
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (Character.isUpperCase(c)) {
				wordsCount++;
			}
		}
		return wordsCount;
	}

	/*
	 * Complete the timeConversion function below.
	 */
	static String timeConversion(String s) {
		if (s.contains("PM")) {
			s = s.replace("PM", " PM");
		} else if (s.contains("AM")) {
			s = s.replace("AM", " AM");
		}

		String time24 = null;
		try {

			// System.out.println("time in 12 hour format : " + now);
			SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm:ss aa");
			SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm:ss");
			time24 = outFormat.format(inFormat.parse(s));

			// System.out.println("time in 24 hour format : " + time24);
		} catch (Exception e) {
			System.out.println("Exception : " + e.getMessage());
		}
		return time24;
	}

	public static void javaMap() {
		Map<String, Integer> map = new HashMap<>();
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		in.nextLine();
		for (int i = 0; i < n; i++) {

			String name = in.nextLine();
			int phone = in.nextInt();
			map.put(name, phone);
			in.nextLine();
		}
		while (in.hasNext()) {
			String s = in.nextLine();
			if (map.get(s) != null) {
				System.out.println(s + "=" + map.get(s));
			} else {
				System.out.println("Not found");
			}

		}
	}

	public static void arrayLeftShift() {
		int shiftPos = 2;
		int[] orgArray = {2, 3, 4, 5, 6};
		int[] newArray = new int[orgArray.length];
		/*
		for (int i = 0; i < shiftPos; i++) {//<-----
			newArray[i + shiftPos - 1] = orgArray[i];
		}
		
		for (int i = 0; i < shiftPos - 1; i++) {//<-----
			newArray[shiftPos - 2 - i] = orgArray[orgArray.length - 1 - i];
		}*/
		List<int[]> asList = Arrays.asList(orgArray);
		Collections.rotate(asList, 3);
		
		System.out.println(asList);
		Utility.printIntegerArray(orgArray);
	}
	
	static int birthdayCakeCandles(int[] arr) {
		System.out.println("here.....");
		int count = 0;
		int size = arr.length;
		int lastElement = arr[size-1];
        for (int i = arr.length - 2; i <= 0; i--) {
			if(lastElement == arr[i] ) {
				count++;
			}
		}
        System.out.println(count);
		return count;
    }

	private static final Scanner scan = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
		// String time = timeConversion("07:05:45PM");
		// System.out.println(camelcase("saveChangesInTheEditor"));
		// System.out.println(minimumNumber(3, "Abm"));
		// javaMap();
		arrayLeftShift();
	}
}
