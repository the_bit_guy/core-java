/**
 * 
 */
package com.corejava.dp.strategy;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface PaymentStrategy {

	public void pay(int amount);
}
