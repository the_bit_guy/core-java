/**
 * 
 */
package com.corejava.dp;

/**
 * @author Bhabadyuti Bal
 * 
 * Strategy pattern is useful when we have multiple algorithms 
 * for specific task and we want our application to be flexible 
 * to chose any of the algorithm at runtime for specific task.
 *
 */
public class StrategyPattern {

}
