/**
 * 
 */
package com.corejava.ds.array;

import com.corejava.utility.Utility;

/**
 * @author Bhabadyuti Bal
 *
 */
public class ArrayReverse {
	
	public static void generalWay(int[] array) {
		int length = array.length;
		int[] reverseArray = new int[length];
		
		for (int i=0;i<length;i++) {
			reverseArray[length - 1 - i ] = array[i];
		}
		Utility.printIntegerArray(reverseArray);
	}
	
	public static void mergeTwoArrays(int[] array1, int[] array2) {
		
	}
	
	public static void main(String[] args) {
		int[] array =  new int[] {1, 2, 3, 4, 5};
		generalWay(array);
	}

}
