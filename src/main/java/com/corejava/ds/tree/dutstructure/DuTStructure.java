/**
 * 
 */
package com.corejava.ds.tree.dutstructure;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.UUID;

import com.corejava.utility.Utility;

/**
 * @author Bhabadyuti Bal
 *
 */
public class DuTStructure {
	
	static Connection dbConn = null;
	static PreparedStatement crunchifyPrepareStat = null;
	
	public DuTStructure() {
		dbConn = Utility.createDBConnection();
		if(dbConn == null) {
			Utility.log("Unable to get mysql connections :(");
			System.exit(0);
		}
	}
	
	
	/*
	 * challenges in DuT Structure
	 * Approach I - Finding all nodes in a structure, for which from any node traversal is required to
	 * find the root node.
	 * Approach II - Having a DuT Structure ID.
	 */
	public boolean createStructure(final String dutId1, final String dutId2, 
			final String dutType1, final String dutType2){
		
		try {
			String insertQry = "INSERT INTO dut_structure values (?, ?, ?, ?, ?)";
			PreparedStatement prepareStatement = dbConn.prepareStatement(insertQry);
			
			prepareStatement.setString(1, UUID.randomUUID().toString());
			prepareStatement.setString(2, dutId1);
			prepareStatement.setString(3, dutType1);
			prepareStatement.setString(4, dutId2);
			prepareStatement.setString(5, dutType2);
			
			prepareStatement.executeUpdate();
			Utility.log("Saved!");
		
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter dutId1 and dutId2 space separated: ");
		String dutId1[] = sc.nextLine().split(" ");
		String dutId2[] = sc.nextLine().split(" ");
		DuTStructure duTStructure = new DuTStructure();
		duTStructure.createStructure(dutId1[0], dutId1[1], dutId2[0], dutId2[1]);
		duTStructure.showDuTStructure();
	}


	private void showDuTStructure() {
		Scanner sc = new Scanner(System.in);
		
	}

}
