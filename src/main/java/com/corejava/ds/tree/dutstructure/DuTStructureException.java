package com.corejava.ds.tree.dutstructure;

public class DuTStructureException extends RuntimeException {
	
	private String message;
	
	public DuTStructureException() {
		super();
	}
	
	public DuTStructureException(String message) {
		super(message);
		this.message = message;
	}
	
	@Override
	public String getMessage() {
        return super.getMessage();
    }

}
